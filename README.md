# Quick PostgreSQL deployment

Project for quick PostgreSQL deployment with [Polemarch](https://github.com/vstconsulting/polemarch). 

Supported OS
------------
This project is able to deploy PostgreSQL on hosts with following OS:

* Ubuntu;
* Debian;
* RedHat;
* CentOS.

Project's playbook
------------------

* **deploy.yml**: This playbook deploys PostgreSQL on all hosts from
selected inventory.

Enjoy it!